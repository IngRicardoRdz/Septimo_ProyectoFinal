package Modelo;

public class ModeloLista {
    
    private int identificador;
    private String nombre, telefono, correo, celular;

    public ModeloLista(int identificador, String nombre, String telefono, String correo, String celular) {
        this.identificador = identificador;
        this.nombre = nombre;
        this.telefono = telefono;
        this.correo = correo;
        this.celular = celular;
    }

    public ModeloLista() {}

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }
    
}
