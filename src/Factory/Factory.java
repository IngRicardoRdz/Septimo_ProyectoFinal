package Factory;

import Modelo.DAO;
import Modelo.ListaDAO;
import Modelo.PrincipalDAO;
import Singleton.Singleton;

public class Factory {
    
    /*Metodo que se le manda a llamar para poder crear alguna clase que se le pida regresar*/
    public static DAO extender(int opcion, Singleton conexion){
        
        if(opcion == 1){
            /*Creamos la clase y le mandamos la conexion para que pueda tener acceso a la base de datos*/
            return new PrincipalDAO(conexion);
        }else if (opcion == 2){
            /*Creamos la clase y le mandamos la conexion para que pueda tener acceso a la base de datos*/
            return new ListaDAO(conexion);
        }
        
        /*Si la opcion no coincide con alguna de las opciones regresara un objeto nulo para que siga trabajando el sistema*/
        return null;
        
    }
    
}
